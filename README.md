![](https://gitlab.com/arcmenu/arcmenu-assets/raw/master/images/azTaskbar.png)

-----
### Introduction

App Icons Taskbar (azTaskbar) is a simple taskbar extension for GNOME Shell, designed to provide a more familiar user experience and workflow. This extension places app icons in the panel showing current running apps, and GNOME favorites.

-----

### Support App Icons Taskbar

App Icons Taskbar is provided free of charge. If you enjoy using this extension and wish to help support the project, feel free to use the link below!

[![Donate via Paypal](https://gitlab.com/arcmenu/arcmenu-assets/raw/master/images/paypal_donate.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=53CWA7NR743WC&item_name=Donate+to+support+my+work&currency_code=USD&source=url)


-----


### Bugs

Bugs should be reported [here](https://gitlab.com/AndrewZaech/aztaskbar/issues) on the GitLab issues page.

When reporting a bug, please provide as much information as you can about the issue and make sure to include your Linux distribution, GNOME Shell version, and App Icons Taskbar version.

-----

### Credits:

**@[AndrewZaech](https://gitlab.com/AndrewZaech) - Project Maintainer and Developer**

Additional credits:
* Large parts of the window preview feature copied from Dash to Dock extension and modified for this extensions needs.

-----
